/* Touch Pad Read Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "esp_log.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/touch_pad.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "blecent.h"

#define TOUCH_PAD_NO_CHANGE   (-1)
#define TOUCH_THRESH_NO_USE   (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (100)

#define TOUCH_CHANNEL       0
#define TOUCH_THRESHOLD     735

#define PIN_LED_R   26
#define PIN_LED_G   27

static int readTouchPad()
{
    static uint32_t avg = 0;
    uint16_t touch_filter_value;

    touch_pad_read_filtered(TOUCH_CHANNEL, &touch_filter_value);

    if(avg == 0)
    {
        avg = touch_filter_value << 8;
    }
    else
    {
        avg-= avg >> 8;
        avg+= touch_filter_value;
    }

    uint16_t thr = (avg - (avg >> 6))>>8;
    printf("Touch: %4d, avg: %4d, thr: %4d\n", touch_filter_value, avg>>8, thr);

    return touch_filter_value < thr;
}

void setup_touch()
{
    // Initialize touch pad peripheral.
    // The default fsm mode is software trigger mode.
    touch_pad_init();
    // Set reference voltage for charging/discharging
    // In this case, the high reference valtage will be 2.7V - 1V = 1.7V
    // The low reference voltage will be 0.5
    // The larger the range, the larger the pulse count value.
    touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
    touch_pad_config(TOUCH_CHANNEL, TOUCH_THRESH_NO_USE);
    touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD);
}

void setup_leds()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = (1ULL<<PIN_LED_R) | (1ULL<<PIN_LED_G);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);
}

typedef enum { Led_Off, Led_Red, Led_Green } Leds_Mode;

void drive_leds(Leds_Mode mode)
{
    gpio_set_level(PIN_LED_R, mode == Led_Red);
    gpio_set_level(PIN_LED_G, mode == Led_Green);
}

void app_main(void)
{

    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init();
    if  (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    setup_leds();
    setup_touch();
    blecent_init();

    while(1)
    {
        drive_leds(Led_Off);

        // Wait for touchpad release
        while(readTouchPad() == 1)
        {
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }

        // Wait for touchpad press
        while(readTouchPad() == 0)
        {
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }

        drive_leds(Led_Green);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        if(readTouchPad() == 0)
        {
            blecent_transmitCommandToAllLighthouses(1);
            continue;
        }

        drive_leds(Led_Red);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        if(readTouchPad() == 0)
        {
            blecent_transmitCommandToAllLighthouses(0);
            continue;
        }
    }
}
